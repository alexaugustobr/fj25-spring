package br.com.caelum.financas.modelo;

public enum TipoMovimentacao {
    ENTRADA("Entrada"), SAIDA("Saida");

    TipoMovimentacao(String nome) {
        this.nome = nome;
    }

    private String nome;

    public String getNome() {
        return nome;
    }
}
