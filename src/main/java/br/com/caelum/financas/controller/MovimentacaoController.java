package br.com.caelum.financas.controller;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/movimentacao")
public class MovimentacaoController {

    private final static List<Conta> contas = List.of(new Conta(1L, "Yuri", "Neon", "123", "123"),
            new Conta(2L, "Lucas", "Inter", "123", "123"));

    @GetMapping("/form")
    public String form(Model model) {

        model.addAttribute("movimentacao", new Movimentacao());
        model.addAttribute("contas", contas);
        return "movimentacao/form";
    }

    @PostMapping("/adiciona")
    public String adicionar(Movimentacao movimentacao) {
        System.out.println(movimentacao);
        return "redirect:/movimentacao/form";
    }
}
