package br.com.caelum.financas.dao;

import br.com.caelum.financas.modelo.Conta;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Transactional
public class ContaDao {

    @PersistenceContext
    private EntityManager manager;

    public void adiciona(Conta conta) {
        manager.persist(conta);
    }
}
